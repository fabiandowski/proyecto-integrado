#### RETRIEVE DATA INFORMATION ON VCENTER ####

data "vsphere_datacenter" "dc" {
  name = "${data.consul_keys.input.var.vsphere_datacenter}"
}

data "vsphere_resource_pool" "pool" {
  # If you haven't resource pool, put "Resources" after cluster name
  name          = "${data.consul_keys.input.var.vsphere_resource_pool}"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_host" "host" {
  name          = "${data.consul_keys.input.var.vsphere_host}"
  datacenter_id = data.vsphere_datacenter.dc.id
}

# Retrieve datastore information on vsphere
data "vsphere_datastore" "datastore" {
  name          = "${data.consul_keys.input.var.vsphere_datastore}"
  datacenter_id = data.vsphere_datacenter.dc.id
}

# Retrieve network information on vsphere
data "vsphere_network" "network" {
  name          = "${data.consul_keys.input.var.vsphere_network}"
  datacenter_id = data.vsphere_datacenter.dc.id
}

# Retrieve template information on vsphere
data "vsphere_virtual_machine" "template" {
  name          = "${data.consul_keys.input.var.vsphere_virtual_machine}"
  datacenter_id = data.vsphere_datacenter.dc.id
}

#### VM Master ####

# Set vm parameters
resource "vsphere_virtual_machine" "test" {
  name             = "${data.consul_keys.input.var.name}-${data.consul_keys.input.var.vsphere_virtual_machine}"
  num_cpus         = "${data.consul_keys.input.var.cpu}"
  memory           = 4096
  datastore_id     = data.vsphere_datastore.datastore.id
  host_system_id   = data.vsphere_host.host.id
  resource_pool_id = data.vsphere_resource_pool.pool.id
  guest_id         = data.vsphere_virtual_machine.template.guest_id
  scsi_type        = data.vsphere_virtual_machine.template.scsi_type

  # Set network parameters
  network_interface {
    network_id = data.vsphere_network.network.id
  }

  # Use a predefined vmware template has main disk
  disk {
    name = "${data.consul_keys.input.var.name}-${data.consul_keys.input.var.vsphere_virtual_machine}.vmdk"
    size = "30"
    thin_provisioned = false
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id

    customize {
      linux_options {
        host_name = "${data.consul_keys.input.var.name}-${data.consul_keys.input.var.vsphere_virtual_machine}"
        domain    = "${data.consul_keys.input.var.domain}"
      }

      network_interface {
        ipv4_address    = "${data.consul_keys.input.var.ip_add}"
        ipv4_netmask    = "${data.consul_keys.input.var.mask}"
        dns_server_list = ["8.8.8.8", "8.8.4.4"]
      }

      ipv4_gateway = "${data.consul_keys.input.var.gateway}"
    }
  }
}


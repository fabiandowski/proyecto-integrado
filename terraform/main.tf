terraform {
  backend "consul" {
    address = "172.19.2.157:8500"
    path = "onpremise/vmwarepod/infraestructura/terraform-state"
 }
}

# Define authentification configuration
provider "vsphere" {
  # if you have a self-signed cert
  allow_unverified_ssl = true
}

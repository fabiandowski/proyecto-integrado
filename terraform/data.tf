variable "consul_base_path" {
  type = "string"
}

data "consul_keys" "input" {

  key {
    name = "vsphere_server"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/vsphere_server"
  }
  key {
    name = "vsphere_user"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/vsphere_user"
  }
  key {
    name = "vsphere_password"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/vsphere_password"
  }
  key {
    name = "vsphere_datacenter"
    path = "${var.consul_base_path}onpremise/vmwarepod//input/vsphere_datacenter"
  }
  key {
    name = "vsphere_resource_pool"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/vsphere_resource_pool"
  }
  key {
    name = "vsphere_host"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/vsphere_host"
  }
  key {
    name = "vsphere_datastore"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/vsphere_datastore"
  }
  key {
    name = "vsphere_datastore"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/vsphere_datastore"
  }
  key {
    name = "vsphere_network"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/vsphere_network"
  }
  key {
    name = "vsphere_network"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/vsphere_network"
  }
  key {
    name = "vsphere_virtual_machine"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/vsphere_virtual_machine"
  }
  key {
    name = "cpu"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/cpu"
  }
  key {
    name = "name"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/name"
  }
  key {
    name = "domain"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/domain"
  }
  key {
    name = "ip_add"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/ip_add"
  }
  key {
    name = "mask"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/mask"
  }
  key {
    name = "gateway"
    path = "${var.consul_base_path}/onpremise/vmwarepod/input/gateway"
  }
 }
